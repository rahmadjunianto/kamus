<script src="<?= base_url() ?>highdmin/plugins/sweet-alert/sweetalert2.min.js"></script>
<script type="text/javascript">
	      //Parameter
        $('#sa-params').click(function () {
            swal({
                title: 'Apakah anda yakin?',
                text: "data yang telah di hapus tidak dapat di kembalikan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, hapus',
                cancelButtonText: 'Tidak , batal!',
                confirmButtonClass: 'btn btn-success mt-2',
                cancelButtonClass: 'btn btn-danger ml-2 mt-2',
                buttonsStyling: false
            }).then(function () {
		            swal({
		                title: 'Deleted !',
		                text: "Your file has been deleted",
		                type: 'success',
		                confirmButtonClass: 'btn btn-confirm mt-2'
		            }
                )
            }, function (dismiss) {
                // dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
                if (dismiss === 'cancel') {
                    swal({
                        title: 'Cancelled',
                        text: "Data batal di hapus :)",
                        type: 'error',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    }
                    )
                }
            })
        });
</script>