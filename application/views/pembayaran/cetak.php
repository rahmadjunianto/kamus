<table border="1" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td width="150px">Telah Terima dari</td>
        <td colspan="3"><?= $data->nama?> ( <?= $data->nama_kelas?> )</td>
        <td align="center"> <?= $data->no_kwitansi?></td>
    </tr>
    <tr>
        <td align="center" rowspan="2">Untuk Pembayaran</td>
        <td align="center" width="10px">No</td>
        <td align="center" colspan="2">Pembayaran</td>
        <td align="center" width="130px">Jumlah</td>
    </tr>
    <tr>
        <td rowspan="1">1</td>
        <td rowspan="1" colspan="2"><?= $data->nm_tagihan?><br>( <?= $data->keterangan?> )</td>
        <td rowspan="1" align="right">Rp <?= angka($data->nominal) ?></td>
    </tr>
    <tr>
        <td>Terbilang</td>
        <td colspan="2"><?= terbilang($data->nominal) ?> Rupiah</td>
        <td width="100px">Jumlah Total</td>
        <td rowspan="1" align="right">Rp <?= angka($data->nominal) ?></td>
    </tr>
</table>
<br>
<table width="100%">
    <tr>
        <td width="50%" align="center"></td>
        <td width="50%" align="center">Ngadiluwih, <?= substr($data->tanggal_bayar,0,10)?></td>
    </tr>
    <tr>
        <td width="50%" align="center"></td>
        <td width="50%" align="center">Penerima</td>
    </tr>
    <tr>
        <td width="50%" align="center"><br><br></td>
        <td width="50%" align="center"><br><br></td>
    </tr>
    <tr>
        <td width="50%" align="center"><br><br></td>
        <td width="50%" align="center"><br><br></td>
    </tr>
    <tr>
        <td width="50%" align="center"><br><br></td>
        <td width="50%" align="center"><br><br></td>
    </tr>
    <tr>
        <td width="50%" align="center"></td>
        <td width="50%" align="center">(____________________________________)</td>
    </tr>
</table>
<style>
    table tr td {
        padding: 5px
    }
</style>