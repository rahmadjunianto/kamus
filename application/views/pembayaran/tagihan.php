<table id="tagihan" class="table table-bordered table-hover">
    <thead>
        <tr>
            <!-- <th style ="width:70px " >Kategori Tagihan</th> -->
            <th style="width:50px ">Tagihan</th>
            <th style="width:60px ">Nominal</th>
            <th style="width:200px ">Keterangan</th>
            <th style="width:50px ">Nominal</th>
            <th style="width:50px ">Keterangan</th>
        </tr>
    </thead>
    <tbody>
        <?php if($tagihan!=null){ ?>

        <?php
                            foreach ($tagihan as $rk)
                            {
                                ?>

        <?php if($rk->status==2){
                                        $cek=$this->db->query("select total_tagihan($rk->kd_piutang) as total_tagihan")->row();
                                        $x=$rk->nominal-$cek->total_tagihan;
                                        ?>

        <?php } else { ?>
        <?php $x=$rk->nominal ?>
        <?php } ?>
        <tr style="cursor: pointer;" class="pilih_tagihan" data-nominal="<?php echo $x ?>"
            data-status="<?php echo $rk->status ?>" data-kd_piutang="<?php echo $rk->kd_piutang ?>">
            <!-- <td style="vertical-align:middle"><?php echo kategori_tagihan($rk->kd_tagihan) ?></td> -->
            <td style="vertical-align:middle"><?php echo $rk->nm_tagihan ?></td>
            <td style="vertical-align:middle"><span><?php echo angka($x) ?></span>
                <?php if($rk->status==2){ ?>
                <span class="cicilan"><button data-kd_piutang="<?php echo $rk->kd_piutang ?>" type="button"
                        class="btn bg-olive margin btn-xs lihat_cicilan">Lihat Cicilan</button></span>
                <?php } ?>


            </td>
            <td style="vertical-align:middle"><?php echo $rk->keterangan ?></td>
            <td>
                <input type="text" onkeyup="formatangka(this);" value="" class="form-control nominal " name="nominal[]"
                    style="text-align:right;" placeholder="Nominal">
            </td>
            <td>
                <input type="text" class="form-control" name="keterangan[]" placeholder="Keterangan">
                <input type="hidden" name="kd_piutang[]" id="kd_piutang" value="<?php echo $rk->kd_piutang; ?>" />
            </td>
        </tr>
        <?php
                            }
                            ?>
        <tr>
            <td colspan="3" align="right">Total</td>
            <td id="jml_bayar" style="text-align:right;padding-right:20px"></td>
            <td></td>
        </tr>

        <?php } else { ?>
        <tr>
            <td colspan="6" align="center">Tidak Ada Tagihan</td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<div class="modal fade" id="myCicilan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Riwayat Cicilan</h4>
            </div>
            <div class="modal-body" id="isi_riwayat">

            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('keyup', '.nominal', function (e) {
        total = $(this).parent().parent().attr('data-nominal');
        jml = $(this).val().replace(/[^\d]/g, "");
        if (parseInt(jml) > parseInt(total)) {
            alert('Jumlah Bayar tidak boleh lebih dari Rp ' + ribuan(total))
            $(this).val('')
        }

        var x = 0;
        $('.nominal').each(function (index, element) {
            if ($(element).val() == '') {
                n = 0
            } else {
                n = $(element).val().replace(/[^\d]/g, "")
            }
            x = x + parseFloat(n);
        });
        $('#jml_bayar').html(ribuan(x));
    });
    $(document).on('click', '.lihat_cicilan', function (e) {

        kd_piutang = $(this).attr('data-kd_piutang');

        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'Pembayaran/getRiwayat'?>",
            data: {
                kd_piutang: kd_piutang
            },
            cache: false,
            success: function (msga) {
                // alert(msga);
                $("#isi_riwayat").html(msga);
                $('#myCicilan').modal('show');
            }
        });
    });
    // $(document).on('click', '.pilih_tagihan', function (e) {
    //     // $('.pilih_tagihan').removeClass('aktif')
    //     // $(this).toggleClass('aktif');
    //     nominal=$(this).attr('data-nominal');
    //     kd_piutang=$(this).attr('data-kd_piutang');
    //     // alert(kd_piutang)
    //     status=$(this).attr('data-status');
    //     $('#kd_piutang').val(kd_piutang)
    //     if(status==0){
    //         $('#total_tagihan').val(nominal)
    //         $('#nominal').val(ribuan(nominal))
    //         $('#metode').val(1);
    //         $('#nominal').prop('readonly', true)
    //         $('#riwayat').hide();
    //     }
    // });
</script>
<style>
    .aktif {
        background: gold !important;
    }
</style>