<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('Mauth');
        $this->load->library('form_validation');
    }

	public function index()
	{
		$this->load->view('formlogin2');
	}

	function proses(){

		if(isset($_POST['real_username']) && isset($_POST['real_password']) ){
			$username = $this->input->post('real_username');
			$password = sha1($this->input->post('real_password'));
			$data     = array('password' => $password,'username' =>$username);
			$res      = $this->Mauth->proseslogin($data);
			if($res=='true'){
				redirect('welcome');

			}else{
				set_flashdata('warning', 'Username atau Password Salah');
				redirect('auth');
			}
		}else{
			// echo "asd";
            set_flashdata('warning', 'Username atau Password Salah');
			redirect('auth');
		}
	}

	function logout(){
		// $this->session->sess_destroy();
		session_destroy();
		set_flashdata('success', 'Berhasil Logout');
		redirect('auth');
	}
	public function getData($val)
	{
		// persiapkan curl
		$ch = curl_init();

		// set url
		curl_setopt($ch, CURLOPT_URL, "http://api.blitarkota.go.id/?data=penduduk&view=pendudukbynik&user=bekti_ccm&citizen_code=".$val);

		// return the transfer as a string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// $output contains the output string
		$output = curl_exec($ch);

		// tutup curl
		curl_close($ch);

		// menampilkan hasil curl
		echo $output;
	}
	function daftar()
	{
		$data = array(
            'title'   =>'Form Pendaftaran Akun',
            'kembali' =>'Auth',
            'action'  => site_url('Auth/daftar_action'),
			'NIK' => set_value('NIK'),
			'PNAMA' => set_value('PNAMA'),
			'PTMPTLHR' => set_value('PTMPTLHR'),
			'PTGLLHR' => set_value('PTGLLHR'),
			'PALAMAT' => set_value('PALAMAT'),
			'PRW' => set_value('PRW'),
			'PKEL' => set_value('PKEL'),
			'PKEC' => set_value('PKEC'),
			'PPEKERJAAN' => set_value('PPEKERJAAN'),
			'PJENKEL' => set_value('PJENKEL'),
			'PNIKAH' => set_value('PNIKAH'),
			'PAGAMA' => set_value('PAGAMA'),
			'PPEND' => set_value('PPEND'),
			'PNOKK' => set_value('PNOKK'),
			'PNMKK' => set_value('PNMKK'),
			'PNMAYAH' => set_value('PNMAYAH'),
			'PNMIBU' => set_value('PNMIBU'),
			'PASSWORD' => set_value('PASSWORD'),
	);
        $this->load->view('daftar', $data);
	}
	function daftar_action()
	{
        // $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->daftar();
        } else {
		$data = array(
			'PNIK' => $this->input->post('NIK',TRUE),
			'PNAMA' => $this->input->post('PNAMA',TRUE),
			'PTMPTLHR' => $this->input->post('PTMPTLHR',TRUE),
			'PTGLLHR' => $this->input->post('PTGLLHR',TRUE),
			'PALAMAT' => $this->input->post('PALAMAT',TRUE),
			'PRW' => $this->input->post('PRW',TRUE),
			'PKEL' => $this->input->post('PKEL',TRUE),
			'PKEC' => $this->input->post('PKEC',TRUE),
			'PPEKERJAAN' => $this->input->post('PPEKERJAAN',TRUE),
			'PJENKEL' => $this->input->post('PJENKEL',TRUE),
			'PNIKAH' => $this->input->post('PNIKAH',TRUE),
			'PAGAMA' => $this->input->post('PAGAMA',TRUE),
			'PPEND' => $this->input->post('PPEND',TRUE),
			'PNOKK' => $this->input->post('PNOKK',TRUE),
			'PNMKK' => $this->input->post('PNMKK',TRUE),
			'PNMAYAH' => $this->input->post('PNMAYAH',TRUE),
			'PNMIBU' => $this->input->post('PNMIBU',TRUE),
			// 'PASSWORD' => $this->input->post('PASSWORD',TRUE),
			);

			$cek=$this->db->query('SELECT count(PNIK) as ID from penduduk where PNIK='.$this->input->post('NIK',TRUE))->row();
			if($cek->ID==0){
				$this->db->trans_start();
				$this->Penduduk_model->insert($data);

				$pengguna = array(
					'nama_lengkap' => $this->input->post('PNAMA',TRUE),
					'username'     => $this->input->post('NIK',TRUE),
					'password'     => sha1($this->input->post('PASSWORD',TRUE)),
					'time_insert'  => date('Y-m-d H:i:s'),
				);
                $this->Pengguna_model->insert($pengguna);
				$insert_id = $this->db->insert_id();

				$dd=array(
					'ms_pengguna_id'=>$insert_id,
					'ms_role_id'=> 6
				);
				$this->db->insert('ms_assign_role',$dd);
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{
					set_flashdata('warning', 'Pendaftaran Gagal.');
				}else{
					set_flashdata('success', 'Pendaftaran Berhasil.');
				}
				redirect(site_url('Auth/Daftar'));
			} else {
				set_flashdata('danger', 'NIK Sudah Terdaftar.');
				redirect(site_url('Auth/Daftar'));
			}
		}
	}
	function _rules()
	{
		$this->form_validation->set_rules('PNAMA', 'Nama', 'trim|required');
		$this->form_validation->set_rules('PTMPTLHR', 'Tempat Lahir', 'trim|required');
		$this->form_validation->set_rules('PTGLLHR', 'Tanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('PALAMAT', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('PRW', 'RW', 'trim|required');
		$this->form_validation->set_rules('PKEL', 'Kelurahan', 'trim|required');
		$this->form_validation->set_rules('PKEC', 'Kecamatan', 'trim|required');
		$this->form_validation->set_rules('PPEKERJAAN', 'Pekerjaan', 'trim|required');
		$this->form_validation->set_rules('PJENKEL', 'Jenis Kelamin', 'trim|required|numeric');
		$this->form_validation->set_rules('PNIKAH', 'Status Pernikahan', 'trim|required');
		$this->form_validation->set_rules('PAGAMA', 'Agama', 'trim|required');
		$this->form_validation->set_rules('PPEND', 'Pendidikan', 'trim|required');
		$this->form_validation->set_rules('PNOKK', 'NO KK', 'trim|required');
		$this->form_validation->set_rules('PNMKK', 'Nama KK', 'trim|required');
		$this->form_validation->set_rules('PNMAYAH', 'Nama Ayah', 'trim|required');
		$this->form_validation->set_rules('PNMIBU', 'Nama Ibu', 'trim|required');
		$this->form_validation->set_rules('PASSWORD', 'Password', 'trim|required');

		$this->form_validation->set_rules('PNIK', 'PNIK', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}
}
