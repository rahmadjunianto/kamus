<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
class Msistem extends CI_Model {

    
    function getRole(){
        return $this->db->get('ms_role');
    }
     
     function getMenu($id){
        $cm=$this->db->query('SELECT id_inc FROM ms_menu')->result_array();
        foreach($cm as $rm){
            $kode_menu=$rm['id_inc'];
            $sql="SELECT ms_menu_id FROM ms_privilege WHERE ms_role_id='$id' AND ms_menu_id='$kode_menu'";
            $qcm=$this->db->query($sql)->num_rows();
            if($qcm == null){
                $data=array('ms_menu_id'=>$kode_menu,'ms_role_id'=>$id);
                $this->db->insert('ms_privilege',$data);
            }
        }

        $qr=$this->db->query("SELECT * FROM (
                                SELECT b.id_inc kode_role,nama_menu,CASE parent WHEN 0 THEN '#'  ELSE NULL END AS parent,status,a.sort sort,b.is_create,b.is_update,b.is_delete
                                FROM ms_menu a , ms_privilege b WHERE a.id_inc=b.ms_menu_id 
                                AND parent=0 AND ms_role_id='$id' 
                                UNION 
                                    SELECT c.id_inc kode_role,a.nama_menu,b.nama_menu parent,status,a.sort ,c.is_create,c.is_update,c.is_delete
                                    FROM ms_menu a,(SELECT id_inc,nama_menu FROM ms_menu) b, ms_privilege c WHERE a.parent=b.id_inc AND a.id_inc=c.ms_menu_id AND ms_role_id='$id' ) cb ORDER BY parent ASC,cb.sort ASC")->result_array();
        return $qr;     
    }


     function prosesrole($data=array()){
        // print_r($data);
        $this->db->trans_start();
        // set null all
        
        $this->db->set('status',null);
        $this->db->set('is_create',null);
        $this->db->set('is_update',null);
        $this->db->set('is_delete',null);
        $this->db->where('ms_role_id',$data['kode_role']);
        $this->db->update('ms_privilege');

        // echo $data['kode_role'];
        // echo "<br>";

        if(!empty($data['read'])){
            $idrr=explode(',',$data['read']);
            $this->db->set('status',1);
            $this->db->where_in('id_inc',$idrr);
            $this->db->update('ms_privilege');
            // echo $this->db->last_query().'<br>';
        }


        if(!empty($data['create'])){
            $idr=explode(',',$data['create']);
            $this->db->set('is_create',1);
            $this->db->where_in('id_inc',$idr);
            $this->db->update('ms_privilege');
        }

        if(!empty($data['update'])){
            $idu=explode(',',$data['update']);
            $this->db->set('is_update',1);
            $this->db->where_in('id_inc',$idu);
            $this->db->update('ms_privilege');
        }
        
        if(!empty($data['delete'])){
            $idd=explode(',',$data['delete']);
            $this->db->set('is_delete',1);
            $this->db->where_in('id_inc',$idd);
            $this->db->update('ms_privilege');
        }


        $this->db->trans_complete();

        return $this->db->trans_status();
    }

}

 